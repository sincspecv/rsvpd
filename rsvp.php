<?php
namespace TheFancyRobot\RSVP;

header("Access-Control-Allow-Origin: http://www.celebrateschroeter.com");

require_once 'bootstrap.php';
use Respect\Validation\Validator as v;

$event = new Event();

$guestName = filter_var(trim($_POST['firstName']), FILTER_SANITIZE_STRING) . " " . filter_var(trim($_POST['lastName']), FILTER_SANITIZE_STRING);
$addGuest = filter_var(trim($_POST['addGuest']), FILTER_SANITIZE_NUMBER_INT);
$eventCode = filter_var(trim($_POST['eventCode']), FILTER_SANITIZE_STRING);
$attending = filter_var(trim($_POST['attending']), FILTER_SANITIZE_STRING);

$guestName = ucwords($guestName);

//Make sure $attending is valid Y or N
if (!v::yes()->validate(ucfirst($attending)) && !v::no()->validate(ucfirst($attending))) {
    echo "\$attending invalid";
    header ('\$attending invalid');
} else if (v::yes()->validate(ucfirst($attending))) {
    $attending = "Y";
} else if (v::no()->validate(ucfirst($attending))) {
    $attending = "N";
}
//insert into db
if (!$event->addToGuestList($eventCode, $guestName, $addGuest, $attending)) {
    echo "Method Failed";
    header('Method Failed', true, 400);
}

