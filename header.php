<?php

ob_start("ob_gzhandler");

session_start();

ini_set('display_errors', 1);

require __DIR__ . '/vendor/autoload.php';

$Loader = (new josegonzalez\Dotenv\Loader(__DIR__ . '/lib/.env'))
              ->parse()
              ->toEnv(); // Throws LogicException if ->parse() is not called first

require_once __DIR__ . '/lib/config.php';

?>


<!DOCTYPE html>
<html>
<head>
  <title> | rsvpd.io | </title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <!-- Bootstrap core CSS -->
    <link href="lib/css/bootstrap_custom.css" rel="stylesheet">


    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Arimo|Monsieur+La+Doulaise" rel="stylesheet">

    <!--- AngularJS
    ================================================== -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"   integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="   crossorigin="anonymous"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.8/angular-ui-router.min.js"></script>
    <script src="//code.angularjs.org/1.5.8/angular-animate.min.js"></script>
    <script type="text/javascript" src="bower_components/angular-validation-match/dist/angular-validation-match.min.js"></script>
    <script type="text/javascript" src="bower_components/angular-smart-table/dist/smart-table.min.js"></script>
    <script type="text/javascript" src="lib/js/ui-bootstrap-tpls-2.2.0.min.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <!--[endif]---->
  </head>

  <body ng-app="rsvpd">

  <?php
  if (!empty($_SESSION)) {
      ?>

      <div ng-controller="navCtrl">
          <nav class="navbar navbar-default" role="navigation">
              <div class="navbar-header">
                  <button type="button" class="navbar-toggle" ng-click="isNavCollapsed = !isNavCollapsed">
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                  </button>
                  <div class="navbar-brand">rsvpd</div>
              </div>

              <div class="collapse navbar-collapse navbar-right" uib-collapse="isNavCollapsed">
                  <ul class="nav navbar-nav">
                      <li><a href="#/dashboard">Dashboard</a></li>
                      <li><a href="#/create">New Event</a></li>
                      <li><a href="account.php">Account</a</li>
                      <li><a href="logout.php">Logout</a></li>
                  </ul>
              </div>
          </nav>
      </div>

  <?php
  }
  ?>

  <div class="container-fluid" id="container">
