<?php
namespace TheFancyRobot\RSVP;

require_once('../bootstrap.php');

use \DrewM\MailChimp\MailChimp;

$MailChimp = new MailChimp('3af653f59b53f6c124a7f052324795e9-us14');

$list_id = 'c1ea2df905';

$result = $MailChimp->post("lists/$list_id/members", [
    'email_address' => 'davy@example.com',
    'status'        => 'subscribed',
]);