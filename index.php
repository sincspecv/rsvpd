<?php
namespace TheFancyRobot\RSVP;
ob_start("ob_gzhandler");

include('bootstrap.php');

?>

<!DOCTYPE html>
<html>
<head>
    <title> rsvpd.io | Get back to the important things!</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" Content="Hosting an event or wedding? Let your guests RSVP by text message. Never waste time or money on mailing RSVP cards, and manage your guest list online." />
    <meta name="robots" contents="index,nofollow" />
    <!-- Bootstrap core CSS -->
    <link href="lib/css/bootstrap_custom.css" rel="stylesheet">


    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Arimo|Monsieur+La+Doulaise" rel="stylesheet">

    <!--- AngularJS
    ================================================== -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"   integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="   crossorigin="anonymous"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.8/angular-ui-router.min.js"></script>
    <script src="//code.angularjs.org/1.5.8/angular-animate.min.js"></script>
    <script type="text/javascript" src="bower_components/angular-validation-match/dist/angular-validation-match.min.js"></script>
    <script type="text/javascript" src="bower_components/angular-smart-table/dist/smart-table.min.js"></script>
    <script type="text/javascript" src="lib/js/ui-bootstrap-tpls-2.2.0.min.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <!--[endif]---->
</head>
<body>
<div class="col-xs-12 landing-wrapper" ng-app="rsvpd">
    <div class="landing-header nav">
        <span><strong>Already have an account? <a href="sign-in.php"> Sign In</a></strong></span>
    </div>
    <div class="hero">
        <div class="hero-header">
            <span class="logo">rsvpd</span>
        </div>
        <div class="squeeze text-center">
            <div class="hook">
                <h1 class="hook-title">Get back to what's important</h1>
                <hr />
                <p class="hook-body">
                    Save time and money. Let your guests RSVP by text message, and never waste time mailing RSVPs.
                </p>
            </div>

            <div class="sign-up" ng-controller="signUpCtrl">
                <div>
                    <h3>Sign up to be notified when we open for registration! </h3>
                    <br />
                </div>
                <div class="sign-up-form">
                    <form role="form" name="regForm" class="" ng-submit="" method="POST" id="register" novalidate>
                        <div class="col-sm-6 col-sm-offset-3">
                            <div class="alert alert-danger" role="alert alert-danger" ng-show="showError">Something went wrong. Please check your entries and try again.</div>
                            <div class="form-group">


                                <!-- EMAIL -->
                                <div ng-show="!success" ng-class="{ 'has-error' : regForm.email.$touched && regForm.email.$invalid }" class="form-group ">
                                    <span class="alert-danger" ng-show="regForm.email.$touched && regForm.email.$invalid">Invalid email address.</span>
                                    <input type="email" class="form-control" name="email" id="email" ng-model="formData.email" placeholder="Email" value="" required>
                                </div>

                                <!-- SUBMIT BUTTON -->
                                <button type="submit" class="btn btn-lg btn-default" data-ng-click="submitForm(regForm.$valid)" ng-disabled="regForm.$invalid">Sign Up!</button>

                                <span class="success" ng-show="success">Thank You! We will let you know as soon as registration is open!</span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="more">
                <a href="#explainer">
                    <span class="btn">
                        <i class="glyphicon glyphicon-menu-down btn-group-lg"></i> <br />
                        <i class="glyphicon glyphicon-menu-down btn-group-lg"></i> <br />
                        <i class="glyphicon glyphicon-menu-down bnt-group-lg"></i> <br />
                    </span>
                </a>
            </div>
        </div>
    </div>
    <div class="explainer col-xs-12" id="explainer">
        <div class="explainer-text col-xs-12">
            <h2>So, what is <span class="logo"> rsvpd </span> ?</h2>
            <h3>Allow your guests to RSVP by text message. See who is coming to your event at any time. Manually add or delete guests from your guest list. Add notes for individual guests. Send a bulk text message to all of your guests.</h3>
        </div>
        <div class="video-container">
            <div class="video" id="video">
            </div>
        </div>
    </div>
</div>

<script src="https://player.vimeo.com/api/player.js"></script>
<script>
    var options = {
        id: 200523613,
        maxwidth: 729,
        maxheight: 402,
        loop: false,
        byline: false,
        portrait: false,
        title: false
    };

    var player = new Vimeo.Player('video', options);

    player.setVolume(0);

    player.on('play', function() {
        console.log('played the video!');
    });
</script>

<script type="text/javascript" src="lib/js/signin.js"></script>
</body>
</html>

<?php ob_end_flush(); ?>
